(define sum
	(lambda(m n s z)
		(m s (n s z))
	)
)

(define subtract
	(lambda(m n)
		(n pred(m))
	)
)

; λn.λf.λx.n (λg.λh.h (g f)) (λu.x) (λu.u)
(define pred
	(lambda(n f x)
		(lambda(g h)(h (g f)))
			(lambda (u)(x))(lambda(u)(u))
		)
	)

;λM N. λa b. N (M a b) b
(define AND
	(lambda(M N a b)
		( N( M a b )b )
	)
)


;λM N. λa b. N a (M a b)
(define OR
	(lambda(M N a b)
	          (N a (M a b))
	)
)

;λM. λa b. M b a
(define NOT
	(lambda( M a b )
		(M b a )
	)
	
)

;λm n. IsZero (Subtract m n)
(define LEQ
	( lambda (m n)
		(IsZero(subtract (m n)))
	)
)

;λn. n (λx. false) true
(define IsZero
	(lambda(n)
		(n(lambda(x)(false)) true)
	)
)

(define GEQ
	(lambda (m n)
		(LEQ(n m) )
	)	
)




